#-------------------------------------------------
#
# Project created by QtCreator 2014-05-06T21:40:18
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = DesenhoDeRede
TEMPLATE = app

# install
target.path = src/plugins/sqldriver
INSTALLS += target

SOURCES += main.cpp\
    src/mainwindow.cpp \
    src/controlapp.cpp \
    src/form/desenhoderede.cpp \
    src/form/operacaodesistemas.cpp \
    src/form/cenario.cpp \
    src/maincontrol.cpp \
    src/model/ngeradordecenarios.cpp \
    src/control/cgeradordecenarios.cpp \
    src/view/vmainwindow.cpp \
    src/view/vdesenhoderede.cpp \
    src/form/widdockcenarios.cpp \
    src/form/widnetwork.cpp \
    src/view/vdockcenarios.cpp \
    src/view/vnetwork.cpp

HEADERS  += src/mainwindow.h \
    src/controlapp.h \
    src/form/desenhoderede.h \
    src/form/operacaodesistemas.h \
    src/form/cenario.h \
    src/maincontrol.h \
    src/control/cgeradordecenarios.h \
    src/model/ngeradordecenarios.h \
    src/view/vmainwindow.h \
    src/view/vdesenhoderede.h \
    src/data/connection.h \
    src/form/widdockcenarios.h \
    src/form/widnetwork.h \
    src/view/vdockcenarios.h \
    src/view/vnetwork.h

FORMS    += mainwindow.ui \
    src/form/desenhoderede.ui \
    src/form/operacaodesistemas.ui \
    src/form/cenario.ui \
    src/form/widdockcenarios.ui \
    src/form/widnetwork.ui

CONFIG += mobility
MOBILITY = 

RESOURCES += \
    resource/resource.qrc

