#include "src/controlapp.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    ControlApp * control = new ControlApp((QObject*) 0);
    Q_UNUSED(control)

    return a.exec();
}
