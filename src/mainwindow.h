#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "src/view/vmainwindow.h"
#include "src/view/vdesenhoderede.h"
#include "src/view/vdockcenarios.h"
#include "src/view/vnetwork.h"

#include <QMainWindow>
#include <QAbstractListModel>
#include <QGraphicsScene>
#include <QSplitter>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

private:
    QSplitter *         m_splitter;

    VMainWindow *       m_vMainWindow;
    VDesenhoDeRede *    m_vDesenhoDeRede;
    VDockCenarios *     m_vDockCenarios;
    VNetwork *          m_vNetwork;

    QGraphicsScene *    m_graphivScene;

private:
    void initUi();

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    //Carregar as informações dos módulos de rede
    void modelComboModulos();
    VMainWindow *vMainWindow();

    //Funções para caregamentos das UIs no cenário principal
    void carregarUiHidrologia();
    void carregarUiCalibracao();
    void carregarUiOperacoesDeSistemas();
    void carregarUiResultado();

signals:
    QStringList signal_modelComboModulos();

private slots:
    void on_clickedToolBarCarregar();
    void on_cbModulos_currentIndexChanged(int a_index);

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
