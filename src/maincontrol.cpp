#include "maincontrol.h"

MainControl::MainControl(QObject *parent) :
    QObject(parent),
    m_cGeradorCenarios(new CGeradorDeCenarios(this))
{
}

CGeradorDeCenarios * MainControl::cGeradorDeCenarios()
{
    return m_cGeradorCenarios;
}
