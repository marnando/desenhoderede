#include "cgeradordecenarios.h"
#include "src/model/ngeradordecenarios.h"

CGeradorDeCenarios::CGeradorDeCenarios(QObject *parent) :
    QObject(parent),
    m_nGeradorDeCenarios(new NGeradorDeCenarios(this))
{
}

CGeradorDeCenarios::~CGeradorDeCenarios()
{
}

QStringList CGeradorDeCenarios::on_modelComboModulos()
{
    QStringList model = QStringList();
    model = QStringList(m_nGeradorDeCenarios->modelComboModulos());

    return model;
}

