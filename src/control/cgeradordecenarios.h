#ifndef CGERADORDECENARIOS_H
#define CGERADORDECENARIOS_H

#include <QObject>
#include <QAbstractListModel>
#include <src/model/ngeradordecenarios.h>

class CGeradorDeCenarios : public QObject
{
    Q_OBJECT

//Envoriments
private:
    NGeradorDeCenarios * m_nGeradorDeCenarios;

//Functions
public:
    explicit CGeradorDeCenarios(QObject *parent = 0);
    virtual ~CGeradorDeCenarios();

signals:

public slots:
    QStringList on_modelComboModulos();

};

#endif // CGERADORDECENARIOS_H
