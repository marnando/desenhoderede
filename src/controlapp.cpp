#include "controlapp.h"
#include "src/maincontrol.h"
#include "src/mainwindow.h"
#include "src/data/connection.h"

#include <QDebug>
#include <QSqlDatabase>
#include <QMessageBox>

ControlApp::ControlApp(QObject *parent) :
    QObject(parent)
{
    //Cria a connexão com o banco
    Connection::createConnection();

//    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
//    db.setDatabaseName(":memory:");
//    if (!db.open()) {
//        QMessageBox::critical(0, tr("Cannot open database"),
//            tr("Unable to establish a database connection.\n"
//                     "This example needs SQLite support. Please read "
//                     "the Qt SQL driver documentation for information how "
//                     "to build it.\n\n"
//                     "Click Cancel to exit."), QMessageBox::Cancel);
//    } else {
//        QMessageBox::critical(0, tr("Open database"),
//            tr("Connected!!"), QMessageBox::Ok);
//    }

    m_mainControl = new MainControl(this);
    on_connectsMainControl();

    m_mainWindow = new MainWindow();
    on_connectsMainWindow();

    iniciarMainWindow();
}

ControlApp::~ControlApp()
{
    if (m_mainWindow) {
        delete m_mainWindow;
        m_mainWindow = NULL;
    }
}

void ControlApp::iniciarMainWindow()
{
    m_mainWindow->modelComboModulos();
    m_mainWindow->show();
}

//Gerencia todos os signals que são emitidos pelas views
void ControlApp::on_connectsMainWindow()
{
    connect(m_mainWindow, SIGNAL(signal_modelComboModulos())
          , m_mainControl->cGeradorDeCenarios(), SLOT(on_modelComboModulos()));
}

//Gerencia todos os signals que são emitidos pelas ctrls
void ControlApp::on_connectsMainControl()
{
}

