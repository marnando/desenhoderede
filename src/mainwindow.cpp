#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QMessageBox>
#include <QHBoxLayout>
#include <QVBoxLayout>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    m_vMainWindow(0),
    m_graphivScene(new QGraphicsScene(this)),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setWindowTitle(tr("Desenho de Rede"));

    initUi();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::initUi()
{
    m_vMainWindow = new VMainWindow(this);

    //Iniciando UIs
    m_vDesenhoDeRede = new VDesenhoDeRede(this);
    m_vDesenhoDeRede->hide();
    on_cbModulos_currentIndexChanged(0);

    m_vDockCenarios = new VDockCenarios(this);
    m_vNetwork = new VNetwork(this);

    //Setandoum layout para o central widget
    QHBoxLayout * hLayout = new QHBoxLayout(this);
    ui->centralWidget->setLayout(hLayout);

    //Dock
    m_splitter = new QSplitter(ui->centralWidget);

    m_splitter->insertWidget(0, m_vDockCenarios);
    m_splitter->insertWidget(1, m_vNetwork);

//    m_splitter->setStretchFactor(0, 30);
//    m_splitter->setStretchFactor(1, 70);

    ui->centralWidget->layout()->addWidget(m_splitter);
}

void MainWindow::modelComboModulos()
{
    QStringList model = QStringList();
    model = signal_modelComboModulos();

    if (!model.isEmpty()) {
//        ui->cbModulos->addItems(model);
    }
}

VMainWindow * MainWindow::vMainWindow()
{
    return m_vMainWindow;
}

void MainWindow::carregarUiHidrologia()
{
}

void MainWindow::carregarUiCalibracao()
{
}

void MainWindow::carregarUiOperacoesDeSistemas()
{
}

void MainWindow::carregarUiResultado()
{

}

void MainWindow::on_clickedToolBarCarregar()
{
    QMessageBox::aboutQt(this);
}

void MainWindow::on_cbModulos_currentIndexChanged(int a_index)
{
    unsigned int modulo = 0;

    //Carregar as informações dos módulos
    modulo = m_vMainWindow->moduloSelecionado(a_index);

    //Caso não consiga carregar as informações dos módulos
    if (modulo == UINT_MAX) {
        return;
    }

    switch (modulo) {
    case MODULO_DESENHO_DE_REDE:
    {
        m_vDesenhoDeRede->carregarUiDesenhoDeRede();

        QWidget * redeUi = new QWidget(this);
        redeUi = m_vDesenhoDeRede->desenhoDeRede();

//        ui->swMainCenarios->insertWidget(0, redeUi);

        redeUi->show();
    }
        break;

    case MODULO_HIDROLOGIA:
        carregarUiHidrologia();
        break;

    case MODULO_CALIBRACAO:
        carregarUiCalibracao();
        break;

    case MODULO_OPERACAO_DE_SISTEMAS:
        carregarUiOperacoesDeSistemas();
        break;

    case MODULO_RESULTADO:
        carregarUiResultado();
        break;

    default:
        break;
    }

    //Tratar
}
