#ifndef WIDNETWORK_H
#define WIDNETWORK_H

#include <QWidget>

namespace Ui {
class WidNetwork;
}

class WidNetwork : public QWidget
{
    Q_OBJECT

public:
    explicit WidNetwork(QWidget *parent = 0);
    ~WidNetwork();

private:
    Ui::WidNetwork *ui;
};

#endif // WIDNETWORK_H
