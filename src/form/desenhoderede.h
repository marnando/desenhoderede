#ifndef DESENHODEREDE_H
#define DESENHODEREDE_H

#include <QWidget>

namespace Ui {
class DesenhoDeRede;
}

class DesenhoDeRede : public QWidget
{
    Q_OBJECT

public:
    explicit DesenhoDeRede(QWidget *parent = 0);
    ~DesenhoDeRede();

    void initUi();

private:
    Ui::DesenhoDeRede *ui;
};

#endif // DESENHODEREDE_H
