#ifndef OPERACAODESISTEMAS_H
#define OPERACAODESISTEMAS_H

#include <QWidget>

namespace Ui {
class OperacaoDeSistemas;
}

class OperacaoDeSistemas : public QWidget
{
    Q_OBJECT

public:
    explicit OperacaoDeSistemas(QWidget *parent = 0);
    ~OperacaoDeSistemas();

private:
    Ui::OperacaoDeSistemas *ui;
};

#endif // OPERACAODESISTEMAS_H
