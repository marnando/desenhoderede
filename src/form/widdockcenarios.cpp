#include "widdockcenarios.h"
#include "ui_widdockcenarios.h"

WidDockCenarios::WidDockCenarios(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::WidDockCenarios)
{
    ui->setupUi(this);
}

WidDockCenarios::~WidDockCenarios()
{
    delete ui;
}
