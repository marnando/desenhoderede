#ifndef CENARIO_H
#define CENARIO_H

#include <QWidget>

namespace Ui {
class Cenario;
}

class Cenario : public QWidget
{
    Q_OBJECT

public:
    explicit Cenario(QWidget *parent = 0);
    ~Cenario();

private:
    Ui::Cenario *ui;
};

#endif // CENARIO_H
