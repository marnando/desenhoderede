#ifndef WIDDOCKCENARIOS_H
#define WIDDOCKCENARIOS_H

#include <QWidget>

namespace Ui {
class WidDockCenarios;
}

class WidDockCenarios : public QWidget
{
    Q_OBJECT

public:
    explicit WidDockCenarios(QWidget *parent = 0);
    ~WidDockCenarios();

private:
    Ui::WidDockCenarios *ui;
};

#endif // WIDDOCKCENARIOS_H
