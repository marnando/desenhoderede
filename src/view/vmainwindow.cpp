#include "vmainwindow.h"

VMainWindow::VMainWindow(QWidget * a_parent) :
    QWidget(a_parent)
{
}

VMainWindow::~VMainWindow()
{
}

unsigned int VMainWindow::moduloSelecionado(int a_index)
{
    unsigned int idModulo = 0;

    switch (a_index) {
    case MODULO_DESENHO_DE_REDE:
        idModulo = MODULO_DESENHO_DE_REDE;
        break;

    case MODULO_HIDROLOGIA:
        idModulo = MODULO_HIDROLOGIA;
        break;

    case MODULO_CALIBRACAO:
        idModulo = MODULO_CALIBRACAO;
        break;

    case MODULO_OPERACAO_DE_SISTEMAS:
        idModulo = MODULO_OPERACAO_DE_SISTEMAS;
        break;

    case MODULO_RESULTADO:
        idModulo = MODULO_RESULTADO;
        break;

    default:
        idModulo = UINT_MAX;
        break;
    }

    return idModulo;
}


