#ifndef VDOCKCENARIOS_H
#define VDOCKCENARIOS_H

#include "src/form/widdockcenarios.h"

#include <QWidget>

class VDockCenarios : public QWidget
{
    Q_OBJECT

private:
    WidDockCenarios * m_widDockCenarios;

public:
    explicit VDockCenarios(QWidget *parent = 0);

signals:

public slots:

};

#endif // VDOCKCENARIOS_H
