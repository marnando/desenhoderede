#ifndef VDESENHODEREDE_H
#define VDESENHODEREDE_H

#include "src/form/desenhoderede.h"

#include <QWidget>

class VDesenhoDeRede : public QWidget
{
    Q_OBJECT

private:
    DesenhoDeRede * m_desenhoDeRede;

public:
    explicit VDesenhoDeRede(QWidget *parent = 0);

    void carregarUiDesenhoDeRede();

    DesenhoDeRede * desenhoDeRede();

signals:

public slots:

};

#endif // VDESENHODEREDE_H
