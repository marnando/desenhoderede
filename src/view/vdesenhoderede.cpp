#include "vdesenhoderede.h"

VDesenhoDeRede::VDesenhoDeRede(QWidget *parent) :
    QWidget(parent)
{
    m_desenhoDeRede = new DesenhoDeRede(this);
}

void VDesenhoDeRede::carregarUiDesenhoDeRede()
{
    m_desenhoDeRede->initUi();
}

DesenhoDeRede *VDesenhoDeRede::desenhoDeRede()
{
    return m_desenhoDeRede;
}
