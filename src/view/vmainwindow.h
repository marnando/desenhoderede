#ifndef VMAINWINDOW_H
#define VMAINWINDOW_H

#define MODULO_DESENHO_DE_REDE          0x0000000ul
#define MODULO_HIDROLOGIA               0x0000001ul
#define MODULO_CALIBRACAO               0x0000002ul
#define MODULO_OPERACAO_DE_SISTEMAS     0x0000003ul
#define MODULO_RESULTADO                0x0000004ul

typedef enum {
} ui_code;

#include <QWidget>

class VMainWindow : public QWidget
{
    Q_OBJECT

public:
    explicit VMainWindow(QWidget * a_parent = 0);
    ~VMainWindow();

    unsigned int moduloSelecionado(int a_index);

signals:

public slots:

};

#endif // VMAINWINDOW_H
