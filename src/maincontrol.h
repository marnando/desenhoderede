#ifndef MAINCONTROL_H
#define MAINCONTROL_H

#include <QObject>
#include <src/control/cgeradordecenarios.h>

class MainControl : public QObject
{
    Q_OBJECT
private:
    CGeradorDeCenarios * m_cGeradorCenarios;

public:
    explicit MainControl(QObject *parent = 0);

    CGeradorDeCenarios * cGeradorDeCenarios();

signals:

public slots:

};

#endif // MAINCONTROL_H
