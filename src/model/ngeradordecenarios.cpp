#include "ngeradordecenarios.h"
#include <QStringList>

NGeradorDeCenarios::NGeradorDeCenarios(QObject * a_parent)
    : QObject(a_parent)
{
}

NGeradorDeCenarios::~NGeradorDeCenarios()
{
}

QStringList NGeradorDeCenarios::modelComboModulos()
{
    return QStringList() << tr("Desenho de Rede")
                         << tr("Hidrologia")
                         << tr("Calibração")
                         << tr("Operações de Sistema")
                         << tr("Resultados");
}
