#ifndef NGERADORDECENARIOS_H
#define NGERADORDECENARIOS_H

#include <QObject>

class NGeradorDeCenarios : public QObject
{
    Q_OBJECT

public:
    NGeradorDeCenarios(QObject * a_parent);
    virtual ~NGeradorDeCenarios();

    QStringList modelComboModulos();
};

#endif // NGERADORDECENARIOS_H
