#ifndef CONTROLAPP_H
#define CONTROLAPP_H

#include <QObject>

class MainWindow;
class MainControl;
class ControlApp : public QObject
{
    Q_OBJECT

public:
    MainWindow *    m_mainWindow; //Classe responsável pelo gerenciamento das views
    MainControl *   m_mainControl; //Classe responsável pelo gerenciamento das controls

public:
    explicit ControlApp(QObject *parent = 0);
    virtual ~ControlApp();

private:
    void iniciarMainWindow();


signals:

public slots:
    //This function is responsable for handling the connects from itself and your children
    void on_connectsMainWindow();

    //This function is responsable for handling the connects from itself and your children
    void on_connectsMainControl();

};

#endif // CONTROLAPP_H
