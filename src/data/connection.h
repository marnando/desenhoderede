#ifndef CONNECTION_H
#define CONNECTION_H

#include <QObject>
#include <QMessageBox>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>

class Connection : public QObject
{
    Q_OBJECT

public:
    static bool createConnection()
    {
        QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
        db.setDatabaseName("teste");
        if (!db.open()) {
            QMessageBox::critical(0
                                , tr("Cannot open database")
                                , tr("Unable to establish a database connection.\n"
                                     "This example needs SQLite support. Please read "
                                     "the Qt SQL driver documentation for information how "
                                     "to build it.\n\n"
                                     "Click Cancel to exit.")
                                , QMessageBox::Cancel);
            return false;
        }

        return true;
    }

};

#endif // CONNECTION_H
